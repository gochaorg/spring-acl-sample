Spring jdbc auth
================

prepare database
----------------

### Запуск mysql контейнера

```bash
$ docker run \
  -d \
  --name spring-acl \
  -v /home/user/code/projects/spring-acl-sample/mysql-data:/var/lib/mysql \
  -p 4306:3306 \
  -e MYSQL_ROOT_PASSWORD=mroot123 \
  -e MYSQL_DATABASE=springsec \
  -e MYSQL_USER=spring \
  -e MYSQL_PASSWORD=spring132q \
  mysql:5.7
```

* `-v /home/user/localdata:/container-dir` - Указание распложения данных mysql
* `-p 4306:3306` - Проброс портов с локального 4306 на контейнер 3306

### Создание таблиц

Создание таблицы пользователей

```sql
CREATE TABLE `user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

Создание таблицы ролей

```sql
CREATE TABLE `user_role` (
  `username` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`username`,`role`),
  CONSTRAINT `user_role2user` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

### Добавление тестовых данных

```sql
INSERT INTO `user` VALUES ('adm','ppp',1),('user','p123',1);
INSERT INTO `user_role` VALUES ('adm','ROLE_ADMIN'),('adm','ROLE_USER'),('user','ROLE_USER');
```
