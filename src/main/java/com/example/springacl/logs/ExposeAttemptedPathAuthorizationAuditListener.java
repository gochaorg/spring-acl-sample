package com.example.springacl.logs;

import com.example.springacl.ReflUtl;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.security.AbstractAuthorizationAuditListener;
import org.springframework.security.access.event.AbstractAuthorizationEvent;
import org.springframework.security.access.event.AuthorizationFailureEvent;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Обработчик/Слушатель событий авторизации/аунтификации для реализации аудита
 */
@Component
public class ExposeAttemptedPathAuthorizationAuditListener extends AbstractAuthorizationAuditListener {
    public static final String AUTHORIZATION_FAILURE
        = "AUTHORIZATION_FAILURE";

    @Override
    public void onApplicationEvent(AbstractAuthorizationEvent event) {
        if (event instanceof AuthorizationFailureEvent) {
            onAuthorizationFailureEvent((AuthorizationFailureEvent) event);
        }
    }

    /**
     * Обабатыает событие аудита, добавляет дополнительную информацию о событии
     * и публикует событие
     * @param event событие
     */
    private void onAuthorizationFailureEvent(
        AuthorizationFailureEvent event) {
        Map<String, Object> data = new HashMap<>();
        data.put(
            "type", event.getAccessDeniedException().getClass().getName());

        data.put("message", event.getAccessDeniedException().getMessage());

        // Определяем к какому URL был доступ (Возможно null)
        if( event.getSource() instanceof FilterInvocation ) {
            data.put(
                "requestUrl",
                ((FilterInvocation) event.getSource()).getRequestUrl()
            );
        }

        // Определяем к какому методу был доступ (в случаи Method security)
        // (Возможно в других версиях Spring будет использоваться другая библиотека AOP)
        if( event.getSource()!=null && event.getSource().getClass().getName().equals("org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation") ){
            Object oMeth = ReflUtl.call(event.getSource(), "getMethod");
            if( oMeth instanceof Method ){
                Method meth = (Method)oMeth;
                data.put("method", meth.toString());
            }
        }

        if (event.getAuthentication().getDetails() != null) {
            data.put(
                "details",
                event.getAuthentication().getDetails()
            );
        }

        publish(new AuditEvent(event.getAuthentication().getName(),
            AUTHORIZATION_FAILURE, data));
    }
}
