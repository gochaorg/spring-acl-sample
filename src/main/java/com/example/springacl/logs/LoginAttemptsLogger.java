package com.example.springacl.logs;

import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.listener.AuditApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

/**
 * Подписчик на события аудита
 */
@Component
public class LoginAttemptsLogger {
    /**
     * Принимает события аудита
     * @param auditApplicationEvent событие аудита
     */
    @EventListener
    public void auditEventHappened(
        AuditApplicationEvent auditApplicationEvent) {
        AuditEvent auditEvent = auditApplicationEvent.getAuditEvent();

        System.out.println("Principal " + auditEvent.getPrincipal()
            + " - " + auditEvent.getType());

        WebAuthenticationDetails details
            = (WebAuthenticationDetails) auditEvent.getData().get("details");

        System.out.println("  Remote IP address: " + details.getRemoteAddress());
        System.out.println("  Session Id: " + details.getSessionId());
        System.out.println("  Request URL: "+ auditEvent.getData().get("requestUrl"));
        if( auditEvent.getData().containsKey("method") ) {
            System.out.println("  Method: " + auditEvent.getData().get("method"));
        }
    }
}
