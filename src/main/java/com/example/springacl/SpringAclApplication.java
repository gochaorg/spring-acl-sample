package com.example.springacl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main класс, для запуска приложения
 */
@SpringBootApplication
public class SpringAclApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringAclApplication.class, args);
	}
}
