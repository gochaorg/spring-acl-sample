package com.example.springacl;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Инструмент для работы с объектами через рефлексю
 */
public class ReflUtl {
    /**
     * Вызов метода объекта
     * @param obj объект
     * @param method имя метода
     * @param args аргументы
     * @return результат вызова
     */
    public static Object call( Object obj, String method, Object ... args){
        Method[] meths = obj.getClass().getMethods();
        Method meth = preffered(meths, method, args);
        if( meth!=null ){
            try {
                return meth.invoke(obj, args);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(ReflUtl.class.getName()).log(Level.SEVERE, null, ex);
                throw new Error(ex);
            }
        }
        throw new Error("method not found: obj("+obj+") method "+method+" with args: "+args);
    }

    /**
     * Поиск подходящих метода среди списка вариантов, согласно указанным аргументам
     * @param constrs список вариантов
     * @param meth имя метода
     * @param args параметры метода
     * @return подходящий метод или null
     */
    public static Method preffered( Method[] constrs, String meth, Object ... args ){
        List<Method> ls = new ArrayList<>();
        ls.addAll(Arrays.asList(constrs));

        while(true){
            if( ls.isEmpty() )break;

            Method remove = null;
            for( Method cons : ls ){
                Class[] ptypes = cons.getParameterTypes();
                if( ptypes.length != args.length ){
                    remove = cons;
                    break;
                }else{
                    if( !isCallableArguments(ptypes, args) ){
                        remove = cons;
                        break;
                    }
                }

                if( remove==null ){
                    if(!cons.getName().equals(meth)){
                        remove = cons;
                        break;
                    }
                }
            }

            if( remove!=null ){
                ls.remove(remove);
                continue;
            }

            if( ls.size()>1 ){
                return null;
            }

            break;
        }

        return ls.size()==1 ? ls.get(0) : null;
    }

    /**
     * Поиск подходящих конструктора среди списка вариантов, согласно указанным аргуметам
     * @param constrs конструкторы
     * @param args аргументы
     * @return подходящий конструктор или null
     */
    public static Constructor preffered(Constructor[] constrs, Object ... args ){
        List<Constructor> ls = new ArrayList<>();
        ls.addAll(Arrays.asList(constrs));

        while(true){
            if( ls.isEmpty() )break;

            Constructor remove = null;
            for( Constructor cons : ls ){
                Class[] ptypes = cons.getParameterTypes();
                if( ptypes.length != args.length ){
                    remove = cons;
                    break;
                }else{
                    if( !isCallableArguments(ptypes, args) ){
                        remove = cons;
                        break;
                    }
                }
            }

            if( remove!=null ){
                ls.remove(remove);
                continue;
            }

            if( ls.size()>1 ){
                return null;
            }

            break;
        }

        return ls.size()==1 ? ls.get(0) : null;
    }

    /**
     * Создание экземпляра класса
     * @param cloader Закгузчик классов
     * @param clsName Имя класса
     * @param args аргументы конструктора класса
     * @return объект
     */
    public static Object newinst( ClassLoader cloader, String clsName, Object ... args ){
        try {
            Class cl = Class.forName(clsName, true, cloader);
            Constructor[] constrs = cl.getConstructors();

            Constructor constr = preffered(constrs, args);
            if( constr==null )throw new Error("preffered constructor not found for class "+clsName);

            return constr.newInstance(args);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(ReflUtl.class.getName()).log(Level.SEVERE, null, ex);
            throw new Error(ex);
        }
    }

    /**
     * Создание экземпляра класса
     * @param cl Класс
     * @param args аргументы конструктора класса
     * @return объект
     */
    public static Object newinst( Class cl, Object ... args ){
        try {
            //Class cl = Class.forName(clsName, true, cloader);
            Constructor[] constrs = cl.getConstructors();

            Constructor constr = preffered(constrs, args);
            if( constr==null )throw new Error("preffered constructor not found for class "+cl.getName());

            return constr.newInstance(args);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(ReflUtl.class.getName()).log(Level.SEVERE, null, ex);
            throw new Error(ex);
        }
    }

    /**
     * Создание массива
     * @param cloader загрузчик классов
     * @param clsName Тип элементов (имя) массива
     * @param items значения массива
     * @return массив
     */
    public static Object array( ClassLoader cloader, String clsName, Object ... items ){
        try {
            Class cls = Class.forName(clsName, true, cloader);
            Object arr = Array.newInstance(cls, items.length);
            for( int i=0; i<items.length; i++ ){
                Array.set(arr, i, items[i]);
            }
            return arr;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReflUtl.class.getName()).log(Level.SEVERE, null, ex);
            throw new Error(ex);
        }
    }

    /**
     * Сверяет на возможность вызова метода с указанными аргументами
     * @param types Типы принимаемых параметорв
     * @param args Параметры
     * @return true - вызвать возможно, false - не возможно вызвать
     */
    public static boolean isCallableArguments(Class[] types,Object[] args)
    {
        if (types == null) {
            throw new IllegalArgumentException("types == null");
        }
        if (args == null) {
            throw new IllegalArgumentException("args == null");
        }

        if (types.length != args.length) {
            return false;
        }

        boolean callable = true;

        for (int paramIdx = 0; paramIdx < types.length; paramIdx++) {
            Class cMethodPrm = types[paramIdx];
            if (args[paramIdx] == null) {
                if( cMethodPrm.isPrimitive() ){
                    callable = false;
                    break;
                }
                continue;
            }

            Class cArg = args[paramIdx].getClass();

            boolean assign = cMethodPrm.isAssignableFrom(cArg);
            if (!assign)
            {
                callable = false;
                break;
            }
        }

        return callable;
    }
}
