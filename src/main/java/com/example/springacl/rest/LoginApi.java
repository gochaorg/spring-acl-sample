package com.example.springacl.rest;

import com.example.springacl.conf.SecurityConfig;
import groovy.util.MapEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.logging.Logger;

/**
 * REST-api возвращает информацию о аунтификации/авторизации текущего пользователя
 */
@RestController
@RequestMapping("/api/login")
public class LoginApi {
    private static final Logger log = Logger.getLogger(LoginApi.class.getName() );

    //<editor-fold desc="userDetailsService">
    private UserDetailsService userDetailsService;

    /**
     * Возвращает ссылку на UserDetailsService - инф о текущем пользователе
     * @return UserDetailsService - инф о текущем пользователе
     */
    public UserDetailsService getUserDetailsService(){ return userDetailsService; }
    @Autowired
    public void setUserDetailsService(UserDetailsService uds){
        this.userDetailsService = uds;
        log.info("assign user detail service: "+userDetailsService);
    }
    //</editor-fold>

    public LoginApi(){
        log.info("created login api");
    }

    /**
     * Возвращает по GET /api/login/status статус текущего пользователя
     * @param request текущий запрос
     * @return JSON объект:
     *
     * <p>В случаи отсуствия аунтификации</p>
     * <code>
     * { <br/>
     * logged: false, <br/>
     * username: null, <br/>
     * authorities: [ ], <br/>
     * userDetailService: "org.springframework.security.provisioning.JdbcUserDetailsManager" <br/>
     * } <br/>
     * </code>
     *
     * <p>В случаи наличия аунтификации</p>
     * <code>
     * { <br/>
     * logged: true, <br/>
     * username: "sec1", <br/>
     * authorities: [ <br/>
     * "ROLE_ADD_USER", <br/>
     * "ROLE_DELETE_USER", <br/>
     * "ROLE_LOCK_USER" <br/>
     * ], <br/>
     * userDetailService: "org.springframework.security.provisioning.JdbcUserDetailsManager" <br/>
     * }
     * </code>
     */
    @RequestMapping(value = "status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object username(HttpServletRequest request){
        String username = request!=null ? request.getUserPrincipal() != null ? request.getUserPrincipal().getName() : null : null;
        List<String> authorities = new ArrayList<>();

        if( userDetailsService!=null && username!=null ){
            UserDetails det = userDetailsService.loadUserByUsername(username);
            if( det!=null ){
                det.getAuthorities().forEach( (GrantedAuthority ga) -> {
                    String a = ga.getAuthority();
                    if( a!=null
                    &&  !a.equals("null") // у пользователя есть явно назначенные роли (таблица user_role), но нет групп (user_usrgroup)
                    ) {
                        authorities.add(a);
                    }
                } );
            }
            //userDetailsService.
        }

        LinkedHashMap<String,Object> map = new LinkedHashMap();
        map.put("logged", username!=null);
        map.put("username", username);
        map.put("authorities", authorities);
        map.put("userDetailService", userDetailsService!=null ? userDetailsService.getClass().getName() : "");

        return map;
    }
}
