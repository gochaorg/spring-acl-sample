package com.example.springacl.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Тестовый REST api
 */
@RestController
@RequestMapping("/api/test")
public class TestApi {
    /**
     * Защита данного метода определена в SecurityConfig
     * @return некое значение
     * @see com.example.springacl.conf.SecurityConfig
     */
    @RequestMapping("hello")
    public Object hello(){
        Map result = new LinkedHashMap();
        result.put("message", "hello");
        return result;
    }

    /**
     * Тестовый метод для проверки безопастности
     * на наличие права <b>ROLE_SUM1</b>
     * @param a число
     * @param b число
     * @return сумма
     */
    @RequestMapping("sum1/{a}/{b}")
    @Secured("ROLE_SUM1")
    public int sum1(@PathVariable("a") int a, @PathVariable("b") int b){
        return a + b;
    }

    /**
     * Тестовый метод для проверки безопастности
     * на наличие права <b>ROLE_SUM2</b>
     * @param a число
     * @param b число
     * @return сумма
     */
    @RequestMapping("sum2/{a}/{b}")
    @RolesAllowed("ROLE_SUM2")
    public int sum2(@PathVariable("a") int a, @PathVariable("b") int b){
        return a + b;
    }

    /**
     * Тестовый метод для проверки безопастности
     * на наличие права <b>ROLE_SUM3</b>
     * @param a число
     * @param b число
     * @return сумма
     */
    @RequestMapping("sum3/{a}/{b}")
    @PreAuthorize("hasRole('ROLE_SUM3')")
    public int sum3(@PathVariable("a") int a, @PathVariable("b") int b){
        return a + b;
    }
}
