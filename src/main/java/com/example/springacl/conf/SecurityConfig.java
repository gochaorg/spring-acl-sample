package com.example.springacl.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;
import java.util.logging.Logger;

/**
 * Описывает базовую конфигурацию безопасности
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static final Logger log = Logger.getLogger(SecurityConfig.class.getName() );

    /**
     * Точка basic аунтификация
     */
    @Autowired
    private AuthenticationEntryPoint authEntryPoint;

    /**
     * Источник данных хранящий сведения о логинах/ролях/...
     */
    @Autowired
    @Qualifier("securityDatasource")
    private DataSource securityDatasource;

    /**
     * Настройка безопастности
     * @param http конфигуратор
     * @throws Exception всевозможные ошибки
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("configure securtiry");

        // Отключаем передачу токена csrf - только больше геморроя чем пользы
        http.csrf().disable();

        // Авторизация запросов
        http.authorizeRequests()
            // Запросы к базовым ресурсам можно для всех пользователей
            .antMatchers("/login","/api/login/**","/css/**").permitAll()
            // Запросы к ресурсам javascript можно для всех пользователей
            .antMatchers("/webjarsjs","/webjars/**").permitAll()
            // Для доступа к API необходио обладать ролью USER (без префикса ROLE_)
            .antMatchers("/api/**").hasRole("USER")
            // Для всех остальных доступ должен быть аунтифицирован
            .anyRequest().authenticated()
            .and()
        // Настройка форм login/logout
        .formLogin()
            // Доступ к странице /login разрешен всем
            .loginPage("/login").permitAll()
            .and()
            // страница logout
            .logout().logoutRequestMatcher(
            new AntPathRequestMatcher("/logout")
            )
            // в момент выхода (logout) информация о сессии должна быть удалена, а сессия закрыта
            .invalidateHttpSession(true).deleteCookies("JSESSIONID").permitAll()
        ;

        // Указываем точку http-basic аунтификации, см AuthenticationEntryPointImpl
        http.httpBasic().authenticationEntryPoint(authEntryPoint);
    }

    /*
        // UserDetailsManager SQL
        public static final String DEF_CREATE_USER_SQL = "insert into users (username, password, enabled) values (?,?,?)";
        public static final String DEF_DELETE_USER_SQL = "delete from users where username = ?";
        public static final String DEF_UPDATE_USER_SQL = "update users set password = ?, enabled = ? where username = ?";
        public static final String DEF_INSERT_AUTHORITY_SQL = "insert into authorities (username, authority) values (?,?)";
        public static final String DEF_DELETE_USER_AUTHORITIES_SQL = "delete from authorities where username = ?";
        public static final String DEF_USER_EXISTS_SQL = "select username from users where username = ?";
        public static final String DEF_CHANGE_PASSWORD_SQL = "update users set password = ? where username = ?";

        // GroupManager SQL
        public static final String DEF_FIND_GROUPS_SQL = "select group_name from groups";
        public static final String DEF_FIND_USERS_IN_GROUP_SQL = "select username from group_members gm, groups g "
            + "where gm.group_id = g.id and g.group_name = ?";
        public static final String DEF_INSERT_GROUP_SQL = "insert into groups (group_name) values (?)";
        public static final String DEF_FIND_GROUP_ID_SQL = "select id from groups where group_name = ?";
        public static final String DEF_INSERT_GROUP_AUTHORITY_SQL = "insert into group_authorities (group_id, authority) values (?,?)";
        public static final String DEF_DELETE_GROUP_SQL = "delete from groups where id = ?";
        public static final String DEF_DELETE_GROUP_AUTHORITIES_SQL = "delete from group_authorities where group_id = ?";
        public static final String DEF_DELETE_GROUP_MEMBERS_SQL = "delete from group_members where group_id = ?";
        public static final String DEF_RENAME_GROUP_SQL = "update groups set group_name = ? where group_name = ?";
        public static final String DEF_INSERT_GROUP_MEMBER_SQL = "insert into group_members (group_id, username) values (?,?)";
        public static final String DEF_DELETE_GROUP_MEMBER_SQL = "delete from group_members where group_id = ? and username = ?";
        public static final String DEF_GROUP_AUTHORITIES_QUERY_SQL = "select g.id, g.group_name, ga.authority "
            + "from groups g, group_authorities ga "
            + "where g.group_name = ? "
            + "and g.id = ga.group_id ";
        public static final String DEF_DELETE_GROUP_AUTHORITY_SQL = "delete from group_authorities where group_id = ? and authority = ?";

        DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY = "select g.id, g.group_name, ga.authority "
			+ "from groups g, group_members gm, group_authorities ga "
			+ "where gm.username = ? " + "and g.id = ga.group_id "
			+ "and g.id = gm.group_id"
     */

    /**
     * Указываем каким образом шифроват пароль
     * @return шифратор пароля
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    private String USER_EXISTS_SQL = "select username from user where username = ?";
    private String USERS_BY_USERNAME_SQL = "select username,password,enabled from user where username=?";
    private String AUTHORITIES_BY_USERNAME_QUERY="select username, role from user_role where username=?";

    private String FIND_GROUPS_SQL="select name from usrgroup";
    private String FIND_USERS_IN_GROUP_SQL=
        "select username " +
        "from usrgroup g " +
        "left join user_usrgroup ug on (ug.usrgroup_id = g.id) " +
        "where g.name = ?";
    private String FIND_GROUP_ID_SQL="select id from usrgroup where name = ?";
    private String GROUP_AUTHORITIES_QUERY_SQL = "select g.id, g.name, rg.role\n" +
        "from usrgroup g\n" +
        "left join role_usrgroup rg on (rg.usrgroup_id = g.id)\n" +
        "where g.name = ?";
    private String GROUP_AUTHORITIES_BY_USERNAME_QUERY = "select g.id, g.name, rg.role\n" +
        "from user u\n" +
        "left join user_usrgroup ug on (ug.username = u.username)\n" +
        "left join usrgroup g on (g.id = ug.usrgroup_id)\n" +
        "left join role_usrgroup rg on (rg.usrgroup_id = g.id)\n" +
        "where u.username = ?";

    /**
     * Конфигурация сервиса UserDetailService в реализации JdbcUserDetailsManager.
     * <p>
     *     Указывает какую бд истопльзовать,
     *     какие запросы слать к базе для управления пользователями/правами/группами пользователей
     * </p>
     * @return JdbcUserDetailsManager в качестве UserDetailService
     */
    @Bean
    public JdbcUserDetailsManager userDetailsManager(){
        DataSource dataSource = securityDatasource;

        if( dataSource!=null ){
            log.info("configure JdbcUserDetailsManager");
        }else{
            log.severe("security datasource not found");
            throw new IllegalStateException("security datasource not found");
        }

        JdbcUserDetailsManager userDetailsManager = new JdbcUserDetailsManager();

        userDetailsManager.setEnableAuthorities(true);
        userDetailsManager.setDataSource(dataSource);
        userDetailsManager.setUserExistsSql(USER_EXISTS_SQL);
        userDetailsManager.setUsersByUsernameQuery(USERS_BY_USERNAME_SQL);
        userDetailsManager.setAuthoritiesByUsernameQuery(AUTHORITIES_BY_USERNAME_QUERY);


        userDetailsManager.setEnableGroups(true);
        userDetailsManager.setFindAllGroupsSql(FIND_GROUPS_SQL);
        userDetailsManager.setFindUsersInGroupSql(FIND_USERS_IN_GROUP_SQL);
        userDetailsManager.setFindGroupIdSql(FIND_GROUP_ID_SQL);
        userDetailsManager.setGroupAuthoritiesSql(GROUP_AUTHORITIES_QUERY_SQL);
        userDetailsManager.setGroupAuthoritiesByUsernameQuery(GROUP_AUTHORITIES_BY_USERNAME_QUERY);

        return userDetailsManager;
    }

    /**
     * Конфигурирует аунтификацию пользователей. <p></p>
     * Использует JdbcUserDetailsManager в качестве аунтификатора.
     * @param auth конфигуратор
     * @throws Exception Ошибки
     */
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        log.info("configure AuthenticationManagerBuilder");

        auth
            .userDetailsService(userDetailsManager())
            .passwordEncoder(passwordEncoder())
        ;
    }
}
