package com.example.springacl.conf;

import com.fasterxml.jackson.databind.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import java.text.SimpleDateFormat;

/**
 * Настраиваем jackson сериализацию
 */
@Configuration
public class JacksonConfig {
    @Bean
    public ObjectMapper objectMapper() {
        return jacksonBuilder().build();
    }

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();

        b.
            indentOutput(true).
            dateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));

        return b;
    }
}
