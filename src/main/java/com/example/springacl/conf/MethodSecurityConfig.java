package com.example.springacl.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

/**
 * Настройки method security.
 *
 * <p>
 * <div style="font-size:18pt; color: #800000;">Ограничения Method security</div>
 *
 * <p>
 *     <ol>
 *         <li>
 * если защищенный метод A вызывается другим методом в том же классе,
 * безопасность в A полностью игнорируется. <br>
 *
 * Это означает, что метод A будет выполняться без какой-либо проверки безопасности. <br>
 *
 * То же самое относится и к частным методам
 *         </li>
 *         <li>
 *             по умолчанию контекст безопасности не распространяется на дочерние потоки. Для получения дополнительной информации, мы можем обратиться к статье распространения контекста Spring Security
 *             <a href="https://www.baeldung.com/spring-security-async-principal-propagation">Spring Security Context Propagation</a>
 *         </li>
 *     </ol>
 */
@Configuration
@EnableGlobalMethodSecurity(
    // Разрешение использовать @PreAuthorize и @PostAuthorize
    prePostEnabled = true,

    // Разрешение использовать @Secured
    securedEnabled = true,

    // Разрешение использовать @RoleAllowed
    jsr250Enabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
}
