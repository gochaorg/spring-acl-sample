package com.example.springacl.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Описываем источник данных хранящий сведения о пользователях и паролях
 */
@Configuration
public class SecurityDatasoureConfig {
    /**
     * Настройки источника определены в файле приложения (application.properties)
     * с префиксом <code>database.security</code>
     * @return источник данных
     */
    @Bean(name = "securityDatasource")
    @ConfigurationProperties(prefix="database.security")
    public DataSource securityDataSource() {
        return DataSourceBuilder.create().build();
    }
}
