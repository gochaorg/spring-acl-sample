package com.example.springacl.controllers;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Настраиваем страницу /login для использоавния teamleaf шаблона (src/resources/templates/login.html)
 */
@Controller
class LoginPageController {
    @RequestMapping("/login")
    public String loginPage(){
        return "login";
    }
}
